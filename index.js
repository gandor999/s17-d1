console.log("Hello");

// Common Examples of Arrays
console.log("\n\nCommon Examples of Arrays --");

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Not recommended use of array

let mixedArr = [12, 'Asus', null, undefined, {}];


// Alternative way to write arrays
let myTasks = [
	'drink HTML',
	'eat Javascript',
	'inhale CSS',
	'bake SASS'
];


// index = n - 1 where n is the total length of array

// Reassigning array values
console.log('Array before reassignment');
console.log(myTasks);

myTasks[0] = 'clean node';

console.log('\nArray After reassignment');
console.log(myTasks);


// Reading from Array
console.log(grades[0]);
console.log(computerBrands[3]);


// Accessing an array element that doesn't exist

console.log(grades[20]);


// Manipulating Arrays
// Array Methods
// Mutator Methods - functions that "mutate" or change an array after they're created
console.log('\n\nManipulating Arrays - Mutators');

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push() - adds an element in the end of an array and returns the array's length.
console.log('Current Array');
console.log(fruits);

let fruitsLength = fruits.push('Mango');

console.log(fruitsLength);
console.log('Mutated array from push method')

console.log(fruits);


// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('\nMutated array from push method - Mutltiple');
console.log(fruits);


// pop() - removes the last element in an array and returns the removed element
console.log('\nMutated array from pop method');

let removedFruit = fruits.pop();

console.log(removedFruit);
console.log(fruits);

// unshift() - adds one or more elements at the beginning of an array
/*
	Syntax:

		arrayName.unshift('elementA');
		arrayName.unshift('elementA', elementB);

*/
console.log('\nMutated array from unshift method');

fruits.unshift('Lime', 'Banana');

console.log(fruits);

// shift() - removes an element at the beginning of an array and returns the removed element.
/*
	Syntax:

		- arrayName.shift();
*/

console.log('\nMutated array from shift method');

let anotherFruit = fruits.shift();

console.log(anotherFruit);
console.log(fruits);



// splice() - simultaneously removes elements from a specified index number and adds elements murag substitute
/*
	Syntax:

		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

console.log('\nMutated array from splice method');

fruits.splice(1, 2, 'Lime', 'Cherry');

console.log(fruits);



// sort() - rearragnes the array elements in alphanumeric order
/*
	Syntax:

		arrayName.sort();
*/

console.log('\nMutated array from sort method');

fruits.sort();

console.log(fruits);



// reverse() - reverses the order of array elements
/*
	Syntax:

		arrayName.reverse();
*/
console.log('\nMutated array from reverse method');

fruits.reverse();

console.log(fruits);


// Non-mutator methods - functions that do not modify or change an array after tjey're created
console.log('\n\n\n\nNon-Mutator Methods');

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf() - returns the index number of the first matching element found in an array
/*
	Syntax:

		arrayName.indexOf(searchValue);
*/
console.log('\nUsing index method');

let firstIndex = countries.indexOf('PH');

console.log(firstIndex);


let invalidCountry = countries.indexOf('BR');

console.log(invalidCountry);



// lastIndexOf() - return the index number of the last matching element found in an array or getting the first index number starting form the last index
/*
	Syntax:

		arrayName.lastIndexOf(searchValue);
*/


console.log('\nUsing lastindex method');

let lastIndex = countries.lastIndexOf('PH');

console.log(lastIndex);



// slice() - portions/slices elements from an array and returns a new array
/*
	Syntax:

		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

console.log('\nUsing slice method');

let slicedArrayA = countries.slice(2);

console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);

console.log(slicedArrayB);

// to be clear ang endingIndex kay iyang kwaon ang the rest starting frm index 4. unya sa tarting iyang e kuha ang elements starting before 2



// toString() - returns an array as a sttring separated by commas
/*
	Syntax:

		arrayName.toString();
*/
console.log('\nUsing toString method');

let stringArray = countries.toString();

console.log(stringArray);



// concat() - combines two arrays and returns the combined result
/*
	Syntax:

		arrayA.concat(arrayB);
*/

console.log('\nUsing concat method');

let taskArrayA = ['drink HTML', 'eat JS'];
let taskArrayB = ['inhale CSS', 'breathe SASS'];
let taskArrayC = ['get Git', 'be Node'];

let tasks = taskArrayA.concat(taskArrayB);

console.log(tasks);


// combining multiple arrays

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);

console.log(allTasks);




// combinging arrays with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');

console.log(combinedTasks);


// join() - returns an array as a string separated by a specified separator string
/*
	Syntax:

		arrayName.join('separatorString');
*/

console.log('\nUsing join method');

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));


//Iteration methods - are loops designed to perform reptitive tasks on arrays

//  - common practice to use the singular form iof the array content for parameter names used in array loops

// forEach() - similar to a for loop that iterates on each array elements

/*
	Syntax:

		arrayName.forEach(function(indiviualElement){
			statements;
		});
*/
console.log('\n\n\n\nIteration Methods');
console.log('\nUsing forEach method');

// you can think of e here as allTasks[i] is to be passed to paramter "e". bale the argument for e is allTasks[i]

allTasks.forEach(function(e){
	console.log(e);
});

console.log('\nUsing forEach method with if statements');


let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task)
	}
});


console.log(filteredTasks);
	


// map() - iterates on each element and returns a new array with different values depending on the result of the function's operation

/*
	Syntax:

		let/const resultArray = arrayName.map(function(indivElement){
			statement/s;
		});
*/

console.log('\nUsing map method');

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number;
});

console.log(numberMap);



// every() - checks if all elements in an array meet the given condition

/*
	Syntax:

		let/const resultArray = arrayName.every(function(indivElement){
			return expression/condition;
		})
*/

console.log('\nUsing every method');

let allValid = numbers.every(function(number){
	return (number < 3);
});

console.log(allValid);



// some() - check if at least one element in the array meets the given condition


/*
	Syntax:

		let/const resultArray = arrayName.some(function(indivElement){
			return expression/condition;
		})
*/

console.log('\nUsing some method');

let someValid = numbers.some(function(number){
	return (number < 2);
});

console.log(someValid);


// filter() - return a new array that conatins elements which meets the given condition

/*
	Syntax:

		let/const resultArray = arrayName.filter(function(indivElement){
			return expression/condition;
		})
*/

console.log('\nUsing filter method');

let filterValid = numbers.filter(function(number){
	return (number < 3);
});

console.log(filterValid);


let nothingFound = numbers.filter(function(number){
	return (number == 0);
});

console.log(nothingFound);

// Filtering using forEach
let filteredNumbers = [];

numbers.forEach(function(number){
	if(number > 3){
		filteredNumbers.push(number);
	}
});

console.log(filteredNumbers);




// includes() - the result of the first method is used on the second method until all "chained" methods have been resolved


// The includes() method returns true if a string contains a specified string, otherwise false.

console.log('\nUsing includes method');


let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
});


console.log(filteredProducts);



// reduce() - evaluates elements from left to right and returns/reduces the array into a single value
/*
	Syntax:

		let/const resultArray = arrayName.reduce(function(accumulator, nextValue){
			return expression/condition;
		})
*/

console.log('\nUsing reduce method');

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('Current Iteration: ' + ++iteration);
	console.log("Accumulator: " + x);
	console.log("Next Value: " + y);

	return x + y;
});


// ang accumulator in the first iteration kay numbers[0] which is atoang x, unya ang next value is numbers[1] which is y. This is al in the first iteration


console.log(reducedArray);

// Reducing string arrays
let list = ['Hello', 'Again', 'World'];
let reducedJoin = list.reduce(function(x, y){
	return x + ' ' + y;
});

console.log(reducedJoin);




// Mutlidimensional Arrays
// Two dimensional array - having an array within an array

console.log('\n\n\n\nMutlidimensional Arrays');

let oneDim = [];

let twoDim = [[2, 4], [6, 8]];

// 2x2 Two Dimensional Array

console.log(twoDim[1][0]);
console.log(twoDim[0][1]);
console.log(twoDim[1][1]);


// 3x2 Two Dimensional Array

let twoDim2 = [[2, 4], [6, 8], [10, 12]];

console.log(twoDim2[2][0]);



